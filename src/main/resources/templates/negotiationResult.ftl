<#include "common/head.ftl">
<div class="am-container am-margin-vertical">
    <h3>协商游戏历史记录</h3>
    <hr/>
    <div class="am-g">
        <div class="am-u-lg-12">
            <table class="am-table am-table-bordered am-table-radius am-table-striped am-table-centered">
                <thead>
                <tr>
                    <th>协商时间</th>
                    <th>协商房间配置文件Id</th>
                    <th>协商房间名称</th>
                    <th>协商个人收益</th>
                    <th>协商总收益</th>
                </tr>
                </thead>
                <tbody>
                <#list negotiationResults as negotiationResult>
                    <tr>
                        <td>${negotiationResult.createTime?datetime}</td>
                        <td>${negotiationResult.roomConfigId}</td>
                        <td>${negotiationResult.roomName}</td>
                        <td>${negotiationResult.resultMap.score}</td>
                        <td>${negotiationResult.resultMap.totalValue}</td>
                    </tr>
                </#list>
                </tbody>
            </table>
        </div>
    </div>
</div>
<#include "common/foot.ftl">