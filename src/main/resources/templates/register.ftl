<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>Register|MHANS</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="format-detection" content="telephone=no">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <link rel="stylesheet" href="http://cdn.amazeui.org/amazeui/2.7.2/css/amazeui.min.css"/>
    <style>
        .header {
            text-align: center;
        }

        .header h1 {
            font-size: 200%;
            color: #333;
            margin-top: 30px;
        }

        .header p {
            font-size: 14px;

        }

        p {
            text-align: center;
        }
    </style>
</head>
<body>
<div class="header">
    <div class="am-g">
        <h1>多边人机在线协商系统</h1>
        <p>Multilateral Human-Machine Online Negotiation System</p>
    </div>
    <hr/>
</div>
<div class="am-g">
    <div class="am-u-lg-6 am-u-md-8 am-u-sm-centered">
        <br>
        <form method="post" class="am-form" action="/register">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}">
            <label for="username">用户名:</label>
            <input type="text" name="username" id="username" value="">
            <label for="password">密码:</label>
            <input type="password" name="password" id="password" value="">
            <label for="nickName">昵称:</label>
            <input type="text" name="nickName" id="nickName" value="">
            <br>
            <div class="am-cf">
                <input type="submit" name="" value="注册账号" class="am-btn am-btn-primary am-btn-block">
            </div>
        </form>
        <hr>
        <p>© 2018 Southwest University.</p>
    </div>
</div>
</body>
</html>
