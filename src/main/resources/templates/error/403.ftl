<#include "../common/head.ftl">
<div class="am-container am-margin-vertical">
    <h3>403-访问受限</h3>
    <hr/>
    <div class="am-g">
        <div class="am-u-sm-12">
            <h2 class="am-text-center am-text-xxxl am-margin-top-lg">403. Restricted ccess</h2>
            <p class="am-text-center">无权限禁止访问</p>
            <pre class="page-404">
          .----.
       _.'__    `.
   .--($)($$)---/#\
 .' @          /###\
 :         ,   #####
  `-..__.-' _.-\###/
        `;_:    `"'
      .'"""""`.
     /,  ya ,\\
    //  403!  \\
    `-._______.-'
    ___`. | .'___
   (______|______)
        </pre>
        </div>
    </div>
</div>
</div>
<#include "../common/foot.ftl">

