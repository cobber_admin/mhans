<#include "common/head.ftl">
<div class="am-container am-margin-vertical" style="min-height: 400px;">
    <h3>房间列表</h3>
    <hr/>
    <div class="am-g">
        <ul class="am-avg-lg-4">
			<#list rooms as room>
                <li>
                    <div class="am-panel am-panel-default am-margin-horizontal">
                        <div class="am-panel-hd">${room.name}</div>
                        <div class="am-panel-bd">
                            <p>${room.roomDescription}</p>
                            <a href="/room/into/${room.roomId}" target="_blank">进入</a>
                        </div>
                    </div>
                </li>
            </#list>
        </ul>
    </div>
</div>
<#include "common/foot.ftl">

