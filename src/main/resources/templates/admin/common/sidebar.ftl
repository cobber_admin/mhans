<div class="admin-sidebar am-offcanvas" id="admin-offcanvas">
    <div class="am-offcanvas-bar admin-offcanvas-bar">
        <ul class="am-list admin-sidebar-list">
            <li><a href="/admin/index"><span class="am-icon-home"></span> 首页</a></li>
            <li><a href="/admin/user/"><span class="am-icon-table"></span> 用户管理</a></li>
            <li><a href="/admin/role/"><span class="am-icon-table"></span> 角色管理</a></li>
            <li><a href="/admin/roomConfig/"><span class="am-icon-table"></span> 房间配置管理</a></li>
            <li><a href="javascript:document.logout.submit()"><span class="am-icon-sign-out"></span> 注销</a></li>
        </ul>

        <div class="am-panel am-panel-default admin-sidebar-panel">
            <div class="am-panel-bd">
                <p><span class="am-icon-bookmark"></span> 公告</p>
                <p></p>
            </div>
        </div>
        <form action="/logout" method="post" name="logout">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}">
        </form>
    </div>
</div>