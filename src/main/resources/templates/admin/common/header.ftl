<header class="am-topbar am-topbar-inverse admin-header">
    <div class="am-topbar-brand">
        <strong>多边人机在线协商系统</strong> <small>后台管理</small>
    </div>

    <button class="am-topbar-btn am-topbar-toggle am-btn am-btn-sm am-btn-success am-show-sm-only" data-am-collapse="{target: '#topbar-collapse'}"><span class="am-sr-only">导航切换</span> <span class="am-icon-bars"></span></button>

    <div class="am-collapse am-topbar-collapse" id="topbar-collapse">
        <ul class="am-nav am-nav-pills am-topbar-nav">
            <li><a href="/">首页</a></li>
            <li class="am-active"><a href="/admin/">管理后台</a></li>
        </ul>
    </div>
</header>