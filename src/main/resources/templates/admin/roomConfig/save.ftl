<!doctype html>
<html class="no-js fixed-layout">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>多边人机在线协商系统-后台管理</title>
    <meta name="description" content="这是一个 index 页面">
    <meta name="keywords" content="index">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp"/>

    <link rel="stylesheet" href="http://cdn.amazeui.org/amazeui/2.7.2/css/amazeui.min.css"/>
    <link rel="stylesheet" href="/assets/css/admin.css">
</head>
<body>
<#include "../common/header.ftl">

<div class="am-cf admin-main">
    <#include "../common/sidebar.ftl">
    <!-- content start -->
    <div class="admin-content">
        <div class="admin-content-body">
            <div class="am-cf am-padding">
                <div class="am-fl am-cf"><strong class="am-text-primary am-text-lg">房间配置管理</strong> /
                    <small>添加配置</small>
                </div>
            </div>
            <div class="am-g">
                <div class="am-u-sm-12">
                    <form class="am-form" action="/admin/roomConfig/save" method="post">
                        <fieldset>
                            <legend>协商房间配置管理</legend>
                            <input type="hidden" name="id" value="${roomConfig.id?if_exists}">
                            <div class="am-form-group">
                                <label for="roomConfigName">房间配置名称</label>
                                <input type="text" class="" id="roomConfigName" name="roomConfigName"
                                       placeholder="房间配置名称" value="${roomConfig.roomConfigName?if_exists}">
                            </div>
                            <div class="am-form-group">
                                <label for="roomNamePrefix">房间实例前缀</label>
                                <input type="text" class="" id="roomNamePrefix" name="roomNamePrefix"
                                       placeholder="房间实例前缀" value="${roomConfig.roomNamePrefix?if_exists}">
                            </div>
                            <div class="am-form-group">
                                <label for="roomBeanName">房间实例Bean名称</label>
                                <input type="text" class="" id="roomBeanName" name="roomBeanName"
                                       placeholder="房间实例Bean名称" value="${roomConfig.roomBeanName?if_exists}">
                            </div>
                            <div class="am-form-group">
                                <label for="roomConfigType">房间实例前缀</label>
                                <select id="roomConfigType" name="roomConfigType">
                                    <option value="normal">一般流程协商</option>
                                </select>
                            </div>
                            <div class="am-form-group">
                                <label for="roomConfigContent">房间配置</label>
                                <textarea class="" rows="10" id="roomConfigContent"
                                          name="roomConfigContent">${roomConfig.roomConfigContent?if_exists}</textarea>
                            </div>
                            <p>
                                <button type="submit" class="am-btn am-btn-default">保存</button>
                                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}">
                            </p>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
        <#include "../common/footer.ftl">
    </div>
    <!-- content end -->

</div>

<!--[if (gte IE 9)|!(IE)]><!-->
<script src="/assets/js/jquery.min.js"></script>
<!--<![endif]-->
<script src="http://cdn.amazeui.org/amazeui/2.7.2/js/amazeui.min.js"></script>
</body>
</html>
