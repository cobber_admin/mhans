<!doctype html>
<html class="no-js fixed-layout">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>多边人机在线协商系统-后台管理</title>
    <meta name="description" content="这是一个 index 页面">
    <meta name="keywords" content="index">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp"/>

    <link rel="stylesheet" href="http://cdn.amazeui.org/amazeui/2.7.2/css/amazeui.min.css"/>
    <link rel="stylesheet" href="/assets/css/admin.css">
</head>
<body>
<#include "../common/header.ftl">

<div class="am-cf admin-main">
    <#include "../common/sidebar.ftl">
    <!-- content start -->
    <div class="admin-content">
        <div class="admin-content-body">
            <div class="am-cf am-padding">
                <div class="am-fl am-cf"><strong class="am-text-primary am-text-lg">房间配置管理</strong> /
                    <small>首页</small>
                </div>
            </div>
            <div class="am-g">
                <div class="am-u-sm-12 am-padding">
                    <div class="am-btn-group">
                        <a class="am-btn am-btn-primary am-radius" href="/admin/roomConfig/save">添加</a>
                    </div>
                    <hr/>
                </div>
                <div class="am-u-sm-12">

                    <table class="am-table">
                        <thead>
                        <tr>
                            <th>id</th>
                            <th>配置名称</th>
                            <th>房间名称前缀</th>
                            <th>房间实例Bean名称</th>
                            <th>房间协商类型</th>
                            <th>操作</th>
                        </tr>

                        </thead>
                        <tbody>
                        <#list roomConfigList as roomConfig>
                        <tr>
                            <td>${roomConfig.id}</td>
                            <td>${roomConfig.roomConfigName}</td>
                            <td>${roomConfig.roomNamePrefix}</td>
                            <td>${roomConfig.roomBeanName}</td>
                            <td>${roomConfig.roomConfigType}</td>
                            <td>
                                <div class="am-btn-group">
                                    <a class="am-btn am-btn-secondary am-radius"
                                       href="/admin/roomConfig/save?id=${roomConfig.id}">详情</a>
                                    <a class="am-btn am-btn-danger am-radius"
                                       href="/admin/roomConfig/delete?id=${roomConfig.id}">删除</a>
                                    <a class="am-btn am-btn-secondary am-radius"
                                       href="javascript:createNewInstance(${roomConfig.id})">实例化</a>
                                </div>
                            </td>
                        </tr>
                        </#list>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <#include "../common/footer.ftl">
    </div>
    <!-- content end -->

</div>

<!--[if (gte IE 9)|!(IE)]><!-->
<script src="/assets/js/jquery.min.js"></script>
<!--<![endif]-->
<script src="http://cdn.amazeui.org/amazeui/2.7.2/js/amazeui.min.js"></script>
<script>
    function createNewInstance(roomConfigId) {
        $.ajax({
            url: "/admin/room/createRoomInstance?roomConfigId=" + roomConfigId,
            success: function (data) {
                if (data.success) {
                    alert("房间实例已创建，房间Id" + data.data.roomId);
                } else {
                    alert("房间创建失败，异常信息：" + data.message);
                }
            }
        });
    }
</script>
</body>
</html>
