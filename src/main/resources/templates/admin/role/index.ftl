<!doctype html>
<html class="no-js fixed-layout">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>多边人机在线协商系统-后台管理</title>
    <meta name="description" content="这是一个 index 页面">
    <meta name="keywords" content="index">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp"/>

    <link rel="stylesheet" href="http://cdn.amazeui.org/amazeui/2.7.2/css/amazeui.min.css"/>
    <link rel="stylesheet" href="/assets/css/admin.css">
</head>
<body>
<#include "../common/header.ftl">

<div class="am-cf admin-main">
    <#include "../common/sidebar.ftl">
    <!-- content start -->
    <div class="admin-content">
        <div class="admin-content-body">
            <div class="am-cf am-padding">
                <div class="am-fl am-cf"><strong class="am-text-primary am-text-lg">角色配置管理</strong> /
                    <small>首页</small>
                </div>
            </div>
            <div class="am-g">
                <div class="am-u-sm-12 am-padding">
                    <button
                            type="button"
                            class="am-btn am-btn-primary"
                            data-am-modal="{target: '#addRole', closeViaDimmer: 0, width: 400, height: 300}">
                        角色添加
                    </button>
                    <hr/>
                </div>
                <div class="am-u-sm-12">

                    <table class="am-table">
                        <thead>
                        <tr>
                            <th>id</th>
                            <th>角色名称</th>
                            <th>操作</th>
                        </tr>

                        </thead>
                        <tbody>
                        <#list roleList as role>
                        <tr>
                            <td>${role.id}</td>
                            <td>${role.roleName}</td>
                            <td>
                                <div class="am-btn-group">
                                    <a class="am-btn am-btn-danger am-radius"
                                       href="/admin/role/delete?roleId=${role.id}">删除</a>
                                </div>
                            </td>
                        </tr>
                        </#list>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <#include "../common/footer.ftl">
    </div>
    <!-- content end -->
    <div class="am-modal am-modal-no-btn" tabindex="-1" id="addRole">
        <div class="am-modal-dialog">
            <div class="am-modal-hd">
                <a href="javascript: void(0)" class="am-close am-close-spin" data-am-modal-close>&times;</a>
            </div>
            <div class="am-modal-bd">
                <form class="am-form" action="/admin/role/add" method="post">
                    <fieldset>
                        <legend>角色添加</legend>
                        <div class="am-form-group">
                            <label for="roleName">角色名称:</label>
                            <input type="text" name="roleName" class="" id="roleName" placeholder="角色名称">
                        </div>
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}">
                        <p>
                            <button type="submit" class="am-btn am-btn-default">添加</button>
                        </p>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>

<!--[if (gte IE 9)|!(IE)]><!-->
<script src="/assets/js/jquery.min.js"></script>
<!--<![endif]-->
<script src="http://cdn.amazeui.org/amazeui/2.7.2/js/amazeui.min.js"></script>
</body>
</html>
