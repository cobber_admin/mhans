<!doctype html>
<html class="no-js fixed-layout">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>多边人机在线协商系统-后台管理</title>
    <meta name="description" content="这是一个 index 页面">
    <meta name="keywords" content="index">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp"/>

    <link rel="stylesheet" href="http://cdn.amazeui.org/amazeui/2.7.2/css/amazeui.min.css"/>
    <link rel="stylesheet" href="/assets/css/admin.css">
</head>
<body>
<#include "../common/header.ftl">

<div class="am-cf admin-main">
    <#include "../common/sidebar.ftl">
    <!-- content start -->
    <div class="admin-content">
        <div class="admin-content-body">
            <div class="am-cf am-padding">
                <div class="am-fl am-cf"><strong class="am-text-primary am-text-lg">用户管理</strong> /
                    <small>用户详情</small>
                </div>
            </div>
            <div class="am-g">
                <div class="am-u-sm-12">
                    <form class="am-form" action="/admin/user/save" method="post">
                        <fieldset>
                            <legend>用户信息</legend>
                            <input type="hidden" name="id" value="${user.id}">
                            <div class="am-form-group">
                                <label for="username">用户名:</label>
                                <input type="text" class="" id="username" name="username"
                                       placeholder="用户名" disabled value="${user.username?if_exists}">
                            </div>
                            <div class="am-form-group">
                                <label for="nickName">用户昵称</label>
                                <input type="text" class="" id="nickName" name="nickName"
                                       placeholder="用户昵称" value="${user.nickName?if_exists}">
                            </div>
                            <div class="am-form-group">
                                <div class="am-form-group">
                                    <h3>用户角色</h3>
                                    <#list roleList as role>
                                        <label class="am-checkbox-inline">
                                            <input type="checkbox" value="${role.id}" name="roleIds" ${user.roles?seq_contains(role)?string("checked","")} data-am-ucheck>
                                            ${role.roleName}
                                        </label>
                                    </#list>
                                </div>
                            </div>
                            <p>
                                <button type="submit" class="am-btn am-btn-default">保存</button>
                                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}">
                            </p>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
        <#include "../common/footer.ftl">
    </div>
    <!-- content end -->

</div>

<!--[if (gte IE 9)|!(IE)]><!-->
<script src="/assets/js/jquery.min.js"></script>
<!--<![endif]-->
<script src="http://cdn.amazeui.org/amazeui/2.7.2/js/amazeui.min.js"></script>
</body>
</html>
