<footer class="am-text-center">
    <hr>
    <p class="am-padding-left am-text-center">© 2018 SouthWest University.</p>
</footer>
<a href="#" class="am-icon-btn am-icon-th-list am-show-sm-only admin-menu"
   data-am-offcanvas="{target: '#admin-offcanvas'}"></a>


<script src="http://cdn.amazeui.org/amazeui/2.7.2/js/amazeui.min.js"></script>
<script src="/assets/js/app.js"></script>
</body>
</html>