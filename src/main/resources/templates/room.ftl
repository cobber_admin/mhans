<html>
<head>
    <title>游戏房间-${room.name}</title>
    <link rel="stylesheet" href="http://cdn.amazeui.org/amazeui/2.7.2/css/amazeui.min.css"/>
    <link rel="stylesheet" href="/assets/css/room.css">
</head>
<body>
<input id="roomId" type="hidden" value="${room.roomId}">
<input id="playerId" type="hidden" value="${Session.SPRING_SECURITY_CONTEXT.authentication.principal.id!''}">
<input id="playerName" type="hidden" value="${Session.SPRING_SECURITY_CONTEXT.authentication.principal.nickName!''}">
<header class="am-topbar am-topbar-inverse am-topbar-fixed-top">
    <h1 class="am-topbar-brand ">
        <strong>多边人机在线协商系统</strong>
        <small>Multilateral Human-Machine Online Negotiation System</small>
    </h1>
    <div class="am-collapse am-topbar-collapse" id="doc-topbar-collapse">
        <div class="am-topbar-right">
            <button class="am-btn am-btn-danger am-topbar-btn am-btn-sm" id="readyButton">准备</button>
            <button class="am-btn am-btn-primary am-topbar-btn am-btn-sm" id="closeButton" style="display:none;">断开
            </button>
        </div>
    </div>
</header>
<div class="main">
    <div class="left">
        <div class="am-panel am-panel-default">
            <div class="am-panel-hd">我的偏好</div>
            <div class="am-panel-bd">
                <table class="am-table am-table-bordered am-table-centered" id="preferencesTable">
                    <tr>
                        <th>议题</th>
                        <th>权重</th>
                        <th>选项</th>
                        <th>分值</th>
                    </tr>
                </table>
            </div>
        </div>
        <div class="am-panel am-panel-default">
            <div class="am-panel-hd">协商参与者</div>
            <div class="am-panel-bd">
                <table class="am-table am-table-bordered am-table-centered" id="playerTable">
                    <tr>
                        <th>昵称</th>
                        <th>类型</th>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="middle">
        <div class="am-panel am-panel-default">
            <div class="am-panel-hd">我来出价</div>
            <div class="am-panel-bd">
                <form class="am-form" id="offerForm">
                    <fieldset>
                    <#list room.issueList as issue>
                        <div class="am-form-group">
                            <label for="doc-select-1">${issue.name}-${issue.description}</label>
                            <select id="doc-select-1" name="${issue.name}" id="issue-${issue.name}">
                                <#list issue.options as option>
                                    <option value="${option.name}">${option.description} | ${option.name}</option>
                                </#list>
                            </select>
                            <span class="am-form-caret"></span>
                        </div>
                    </#list>
                        <p>
                            <button type="button" class="am-btn am-btn-primary" id="offerBtn" disabled="disabled">出价
                            </button>
                        </p>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
    <div class="right">
        <div class="am-panel am-panel-default">
            <div class="am-panel-hd">实时交流</div>
            <div class="am-panel-bd">
                <textarea class="chatMessageList" rows="5" id="chatMessageList" disabled="disabled"></textarea>
                <hr/>
                <div class="am-input-group">
                    <input type="text" class="am-form-field" id="chatMessageInput">
                    <span class="am-input-group-btn">
                        <button class="am-btn am-btn-primary" type="button" id="chatMessageButton">发送</button>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="am-modal am-modal-confirm" tabindex="-1" id="voteConfirm">
    <div class="am-modal-dialog">
        <div class="am-modal-hd" id="voteConfirmPlayerName"></div>
        <div class="am-modal-bd">
            <h4>出价结果：</h4>
            <ul class="am-list am-list-static" id="voteConfirmList">
            </ul>
            <h4>出价得分：</h4>
            <h3 id="voteConfirmScore"></h3>
        </div>
        <div class="am-modal-footer">
            <span class="am-modal-btn" data-am-modal-cancel>拒绝</span>
            <span class="am-modal-btn" data-am-modal-confirm>接受</span>
        </div>
    </div>
</div>
<script src="/assets/js/jquery.min.js"></script>
<script src="http://cdn.amazeui.org/amazeui/2.7.2/js/amazeui.min.js"></script>
<script src="/assets/js/room.js"></script>
</body>
</html>