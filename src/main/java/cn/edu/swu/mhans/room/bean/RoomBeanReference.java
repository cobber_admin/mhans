package cn.edu.swu.mhans.room.bean;

import lombok.Data;

/**
 * @author ZhanJingbo
 * @version 1.0.0
 * Created on 2018/3/26
 */
@Data
public class RoomBeanReference {

    private String referenceRoomBeanName;

    public RoomBeanReference(String referenceRoomBeanName) {
        this.referenceRoomBeanName = referenceRoomBeanName;
    }
}
