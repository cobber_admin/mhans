package cn.edu.swu.mhans.room.dao.impl;

import cn.edu.swu.mhans.room.dao.RoomDao;
import cn.edu.swu.mhans.room.share.base.BaseRoom;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

/**
 * @author ZhanJingbo
 * @version 1.0.0
 * Created on 2018/5/9
 */
@Repository
public class RoomDaoWithRedis implements RoomDao {

    private static final String ROOM_KEY_PREFIX = "mhans:room:";

    @Autowired
    private RedisTemplate<String, BaseRoom> roomRedisTemplate;


    /**
     * 添加baseRoom
     *
     * @param baseRoom baseRoom
     */
    @Override
    public void addBaseRoom(BaseRoom baseRoom) {
        roomRedisTemplate.opsForValue().set(ROOM_KEY_PREFIX + baseRoom.getRoomId(), baseRoom);
    }

    /**
     * 获取baseRoom
     *
     * @param roomId room的id
     * @return
     */
    @Override
    public BaseRoom getBaseRoom(String roomId) {
        return roomRedisTemplate.opsForValue().get(ROOM_KEY_PREFIX + roomId);
    }

    /**
     * 获取现有BaseRoom的列表
     *
     * @return BaseRoom的列表
     */
    @Override
    public List<BaseRoom> findAll() {
        Set<String> keySet = roomRedisTemplate.keys(ROOM_KEY_PREFIX + "*");
        return roomRedisTemplate.opsForValue().multiGet(keySet);
    }

    /**
     * 删除房间
     *
     * @param roomId 房间id
     */
    @Override
    public void deleteBaseRoom(String roomId) {
        roomRedisTemplate.delete(ROOM_KEY_PREFIX + roomId);
    }
}
