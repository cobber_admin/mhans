package cn.edu.swu.mhans.room.bean;

import java.util.Map;

/**
 * @author ZhanJingbo
 * @version 1.0.0
 * Created on 2018/3/26
 */
public interface RoomBeanDefinitionReader {
    /**
     * 加载对应资源中的bean定义
     *
     * @param locations
     */
    void loadRoomBeanDefinitions(String locations);

    /**
     * 获取加载的bean定义
     *
     * @return bean定义Map
     */
    Map<String, RoomBeanDefinition> getRoomBeanDefinition();
}
