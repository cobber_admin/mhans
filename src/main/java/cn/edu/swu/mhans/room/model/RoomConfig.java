package cn.edu.swu.mhans.room.model;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class RoomConfig implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * id
     */
    private Long id;
    /**
     * 配置名
     */
    private String roomConfigName;
    /**
     * 配置类型
     */
    private String roomConfigType;
    /**
     * 房间名称
     */
    private String roomNamePrefix;
    /**
     * roomBean名称
     */
    private String roomBeanName;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 修改时间
     */
    private Date modifyTime;

    /**
     * 扩展字段
     */
    private String ext;
    /**
     * 配置内容
     */
    private String roomConfigContent;


}