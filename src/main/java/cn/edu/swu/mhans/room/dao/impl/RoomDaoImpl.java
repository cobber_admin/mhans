package cn.edu.swu.mhans.room.dao.impl;

import cn.edu.swu.mhans.room.dao.RoomDao;
import cn.edu.swu.mhans.room.share.base.BaseRoom;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author ZhanJingbo
 * @version 1.0.0
 * Created on 2018/4/11
 */

public class RoomDaoImpl implements RoomDao {

    private Map<String, BaseRoom> roomMap = new ConcurrentHashMap<>();

    /**
     * 添加baseRoom
     *
     * @param baseRoom baseRoom
     */
    @Override

    public void addBaseRoom(BaseRoom baseRoom) {
        roomMap.put(baseRoom.getRoomId(), baseRoom);
    }

    /**
     * 获取baseRoom
     *
     * @param roomId room的id
     * @return
     */
    @Override
    public BaseRoom getBaseRoom(String roomId) {
        return roomMap.get(roomId);
    }

    /**
     * 获取现有BaseRoom的列表
     *
     * @return BaseRoom的列表
     */
    @Override
    public List<BaseRoom> findAll() {
        return new ArrayList<>(roomMap.values());
    }

    /**
     * 删除房间
     *
     * @param roomId 房间id
     */
    @Override
    public void deleteBaseRoom(String roomId) {
        roomMap.remove(roomId);
    }
}
