package cn.edu.swu.mhans.room.bean.io;

import lombok.Data;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;


/**
 * @author ZhanJingbo
 * @version 1.0.0
 * Created on 2018/3/24
 */
@Data
public class UrlResource implements Resource {


    private final URL url;

    public UrlResource(URL url) {
        this.url = url;
    }

    /**
     * 获取输入流
     *
     * @return 输入流
     */
    @Override
    public InputStream getInputStream() {
        try {
            URLConnection urlConnection = url.openConnection();
            urlConnection.connect();
            return urlConnection.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
