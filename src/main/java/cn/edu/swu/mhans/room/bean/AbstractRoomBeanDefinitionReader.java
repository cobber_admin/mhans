package cn.edu.swu.mhans.room.bean;

import cn.edu.swu.mhans.room.bean.io.ResourceLoader;

import java.util.HashMap;
import java.util.Map;

/**
 * @author ZhanJingbo
 * @version 1.0.0
 * Created on 2018/3/26
 */
public abstract class AbstractRoomBeanDefinitionReader implements RoomBeanDefinitionReader {


    private final ResourceLoader resourceLoader;

    private Map<String, RoomBeanDefinition> roomBeanDefinitionMap;

    protected AbstractRoomBeanDefinitionReader(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
        roomBeanDefinitionMap = new HashMap<>();
    }

    /**
     * 获取加载的bean定义
     *
     * @return bean定义Map
     */
    @Override
    public Map<String, RoomBeanDefinition> getRoomBeanDefinition() {
        return this.roomBeanDefinitionMap;
    }

    public ResourceLoader getResourceLoader() {
        return resourceLoader;
    }
}
