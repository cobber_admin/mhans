package cn.edu.swu.mhans.room.service;

import cn.edu.swu.mhans.common.service.BaseService;
import cn.edu.swu.mhans.room.model.RoomConfig;

import java.util.List;

/**
 * 协商房间配置服务
 *
 * @author ZhanJingbo
 * @version 1.0.0
 * Created on 2018/4/9
 */
public interface RoomConfigService extends BaseService<RoomConfig, Long> {

}
