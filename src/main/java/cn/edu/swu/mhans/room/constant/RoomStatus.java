package cn.edu.swu.mhans.room.constant;

import lombok.Getter;

/**
 * @author ZhanJingbo
 * @version 1.0.0
 * Created on 2018/5/9
 */

public enum RoomStatus {
    /**
     * 准备中
     */
    READY(0),
    /**
     * 游戏中
     */
    IN_THE_GAME(1),
    /**
     * 结束
     */
    END(2);

    @Getter
    private int value;

    RoomStatus(int value) {
        this.value = value;
    }
}
