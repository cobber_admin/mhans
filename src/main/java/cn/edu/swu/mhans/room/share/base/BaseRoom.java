package cn.edu.swu.mhans.room.share.base;

import cn.edu.swu.mhans.common.player.NegotiationPlayer;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 房间的统一基类
 *
 * @author ZhanJingbo
 * @version 1.0.0
 * Created on 2018/3/12
 */
@Data
public class BaseRoom implements Serializable {
    /**
     * 房间唯一标识Id
     */
    private String roomId;

    /**
     * 对应配置文件Id
     */
    private Long roomConfigId;

    /**
     * 房间名称
     */
    private String name;

    /**
     * 房间描述
     */
    private String roomDescription;
    /**
     * 房间协商参与人数（包含所有协商参与者）
     */
    private Integer totalPlayerCount;

    /**
     * 协商参与者
     */
    private List<NegotiationPlayer> negotiationPlayers;


    /**
     * 房间状态：0 未开始，1 游戏中，2游戏已结束
     */
    private Integer roomStatus;

    /**
     * 房间类型
     */
    private String roomType;

    public BaseRoom() {
        roomStatus = 0;
    }
}
