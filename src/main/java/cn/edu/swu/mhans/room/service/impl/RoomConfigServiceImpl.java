package cn.edu.swu.mhans.room.service.impl;

import cn.edu.swu.mhans.room.dao.RoomConfigMapper;
import cn.edu.swu.mhans.room.model.RoomConfig;
import cn.edu.swu.mhans.room.service.RoomConfigService;
import cn.edu.swu.mhans.common.util.FieldUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author ZhanJingbo
 * @version 1.0.0
 * Created on 2018/4/9
 */
@Service
public class RoomConfigServiceImpl implements RoomConfigService {


    @Resource
    private RoomConfigMapper roomConfigMapper;

    /**
     * 添加
     *
     * @param roomConfig 实体
     * @return 是否添加成功
     */
    @Override
    public boolean add(RoomConfig roomConfig) {
        if (null == roomConfig) {
            return false;
        }
        return roomConfigMapper.insert(roomConfig) != 0;
    }

    /**
     * 根据id字段对其它非空字段进行修改
     *
     * @param roomConfig 实体
     * @return 是否修改成功
     */
    @Override
    public boolean update(RoomConfig roomConfig) {
        RoomConfig entity = roomConfigMapper.selectByPrimaryKey(roomConfig.getId());
        FieldUtils.copyFileds(roomConfig, entity);
        return roomConfigMapper.updateByPrimaryKey(entity) != 0;
    }

    /**
     * 根据id删除
     *
     * @param id 实体的id
     * @return 是否删除成功
     */
    @Override
    public boolean delete(Long id) {
        if (null == id) {
            return false;
        }
        return roomConfigMapper.deleteByPrimaryKey(id) != 0;
    }

    /**
     * 根据id获取实体
     *
     * @param id 实体
     * @return 查找实体
     */
    @Override
    public RoomConfig findOne(Long id) {
        return roomConfigMapper.selectByPrimaryKey(id);
    }

    /**
     * 获取所有对象
     *
     * @return 对象列表
     */
    @Override
    public List<RoomConfig> findAll() {
        return roomConfigMapper.selectAll();
    }
}
