package cn.edu.swu.mhans.room.dao;

import cn.edu.swu.mhans.room.share.base.BaseRoom;

import java.util.List;

/**
 * @author ZhanJingbo
 * @version 1.0.0
 * Created on 2018/4/11
 */
public interface RoomDao {
    /**
     * 添加baseRoom
     *
     * @param baseRoom baseRoom
     */
    void addBaseRoom(BaseRoom baseRoom);

    /**
     * 获取baseRoom
     *
     * @param roomId room的id
     * @return
     */
    BaseRoom getBaseRoom(String roomId);

    /**
     * 获取现有BaseRoom的列表
     *
     * @return BaseRoom的列表
     */
    List<BaseRoom> findAll();

    /**
     * 删除房间
     *
     * @param roomId 房间id
     */
    void deleteBaseRoom(String roomId);
}
