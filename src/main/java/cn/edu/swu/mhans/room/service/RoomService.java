package cn.edu.swu.mhans.room.service;

import cn.edu.swu.mhans.common.service.BaseService;
import cn.edu.swu.mhans.room.share.base.BaseRoom;

/**
 * @author ZhanJingbo
 * @version 1.0.0
 * Created on 2018/4/10
 */
public interface RoomService extends BaseService<BaseRoom, String> {

    /**
     * 通过id获取RoomConfig创建房间实例
     *
     * @param roomConfigId roomConfig的Id
     * @return 房间实例
     * @throws Exception 错误
     */
    BaseRoom createRoomInstanceByRoomConfigId(Long roomConfigId) throws Exception;
}
