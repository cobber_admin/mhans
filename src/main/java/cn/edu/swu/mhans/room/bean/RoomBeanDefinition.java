package cn.edu.swu.mhans.room.bean;

import lombok.Data;

/**
 * @author ZhanJingbo
 * @version 1.0.0
 * Created on 2018/3/24
 */
@Data
public class RoomBeanDefinition {

    /**
     * roomBean的类对象
     */
    private Class roomBeanClass;
    /**
     * rooBean的名称
     */
    private String roomBeanName;

    /**
     * 参数值
     */
    private PropertyValues propertyValues;

    /**
     * 根据配置生成的对象
     */
    private Object roomBean;

}
