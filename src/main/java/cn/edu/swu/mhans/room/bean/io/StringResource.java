package cn.edu.swu.mhans.room.bean.io;

import lombok.Data;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

/**
 * 字符串资源
 *
 * @author ZhanJingbo
 * @version 1.0.0
 * Created on 2018/3/23
 */
@Data
public class StringResource implements Resource {

    private String str;

    public StringResource(String str) {
        this.str = str;
    }

    /**
     * 获取输入流
     *
     * @return 输入流
     */
    @Override
    public InputStream getInputStream() {
        return new ByteArrayInputStream(str.getBytes());
    }
}
