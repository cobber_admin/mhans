package cn.edu.swu.mhans.room.bean.io;

/**
 * @author ZhanJingbo
 * @version 1.0.0
 * Created on 2018/3/24
 */
public interface ResourceLoader {

    /**
     * 通过参数获取Resource对象
     *
     * @param resourceString resouce对象的字符串
     * @return Resource对象
     */
    Resource getResource(String resourceString);
}
