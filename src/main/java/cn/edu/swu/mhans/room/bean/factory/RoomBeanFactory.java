package cn.edu.swu.mhans.room.bean.factory;

import cn.edu.swu.mhans.room.bean.RoomBeanDefinition;

/**
 * 对象工厂接口
 *
 * @author ZhanJingbo
 * @version 1.0.0
 * Created on 2018/3/26
 */
public interface RoomBeanFactory {

    /**
     * 获取一个对象实例
     *
     * @param roomBeanName bean的名称
     * @return 对象实例
     * @throws Exception 异常
     */
    Object getRoomBean(String roomBeanName) throws Exception;

    void registerRoomBeanDefinition(String name, RoomBeanDefinition roomBeanDefinition);

}
