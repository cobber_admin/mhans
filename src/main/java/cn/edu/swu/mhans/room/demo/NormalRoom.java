package cn.edu.swu.mhans.room.demo;

import cn.edu.swu.mhans.common.player.NegotiationPlayer;
import cn.edu.swu.mhans.room.demo.issue.DiscreteIssue;
import cn.edu.swu.mhans.room.demo.role.PlayerPreferences;
import cn.edu.swu.mhans.room.share.base.BaseRoom;
import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * 一般流程展示房间
 *
 * @author ZhanJingbo
 * @version 1.0.0
 * Created on 2018/4/29
 */
@Data
public class NormalRoom extends BaseRoom {

    /**
     * 当前出价人
     */
    private int currentMc;

    /**
     * 一般流程待讨论议题
     */
    private List<DiscreteIssue> issueList;

    /**
     * 游戏玩家偏好
     */
    private List<PlayerPreferences> playerPreferencesList;

    /**
     * 表决状态
     */
    private boolean[] voteStatus;
    /**
     * 当前投票人数
     */
    private int voteCount;

    /**
     * 当前出价
     */
    private Map<String, Object> currentOfferData;
}
