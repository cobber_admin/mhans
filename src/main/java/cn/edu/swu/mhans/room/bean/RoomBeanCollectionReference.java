package cn.edu.swu.mhans.room.bean;

import lombok.Data;


/**
 * @author ZhanJingbo
 * @version 1.0.0
 * Created on 2018/3/27
 */
@Data
public class RoomBeanCollectionReference {

    /**
     * 集合类型，0:List,1:Map
     */
    private Integer collectionType;
    /**
     * 元素的类型
     */
    private String itemTypeClassName;
    /**
     * 对应的集合描述的对象
     */
    private Object collectionObject;


    public enum RoomBeanCollectionReferenceType {
        /**
         * 链表
         */
        LIST(0),
        /**
         * 键值对
         */
        MAP(1);
        private int value;

        RoomBeanCollectionReferenceType(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    public RoomBeanCollectionReference(Integer collectionType, String itemTypeClassName, Object collectionObject) {
        this.collectionType = collectionType;
        this.itemTypeClassName = itemTypeClassName;
        this.collectionObject = collectionObject;
    }
}
