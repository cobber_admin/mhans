package cn.edu.swu.mhans.room.bean.io;

import java.io.InputStream;

/**
 * 配置文件资源统一接口
 * @author ZhanJingbo
 * @version 1.0.0
 * Created on 2018/3/23
 */
public interface Resource {
    /**
     * 获取输入流
     * @return 输入流
     */
    InputStream getInputStream();
}
