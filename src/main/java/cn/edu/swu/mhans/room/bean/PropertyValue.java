package cn.edu.swu.mhans.room.bean;

import lombok.Data;

/**
 * @author ZhanJingbo
 * @version 1.0.0
 * Created on 2018/3/24
 */
@Data
public class PropertyValue {

    private final String name;
    private final Object value;

    public PropertyValue(String name, Object value) {
        this.name = name;
        this.value = value;
    }

}
