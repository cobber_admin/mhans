package cn.edu.swu.mhans.room.dao;

import cn.edu.swu.mhans.room.model.RoomConfig;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface RoomConfigMapper {
    int deleteByPrimaryKey(Long id);

    int insert(RoomConfig record);

    RoomConfig selectByPrimaryKey(Long id);

    List<RoomConfig> selectAll();

    int updateByPrimaryKey(RoomConfig record);
}