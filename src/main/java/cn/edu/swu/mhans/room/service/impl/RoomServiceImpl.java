package cn.edu.swu.mhans.room.service.impl;

import cn.edu.swu.mhans.common.util.FieldUtils;
import cn.edu.swu.mhans.common.util.IdUtils;
import cn.edu.swu.mhans.room.bean.AbstractRoomBeanDefinitionReader;
import cn.edu.swu.mhans.room.bean.factory.AbstractRoomBeanFactory;
import cn.edu.swu.mhans.room.bean.factory.AutowireCapableRoomBeanFactory;
import cn.edu.swu.mhans.room.bean.io.DefaultResourceLoader;
import cn.edu.swu.mhans.room.bean.xml.XmlRoomBeanDefinitionReader;
import cn.edu.swu.mhans.room.dao.RoomDao;
import cn.edu.swu.mhans.room.model.RoomConfig;
import cn.edu.swu.mhans.room.service.RoomConfigService;
import cn.edu.swu.mhans.room.service.RoomService;
import cn.edu.swu.mhans.room.share.base.BaseRoom;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author ZhanJingbo
 * @version 1.0.0
 * Created on 2018/4/10
 */
@Service
public class RoomServiceImpl implements RoomService {

    @Resource
    private RoomConfigService roomConfigService;
    @Resource
    private RoomDao roomDao;

    /**
     * 通过id获取RoomConfig创建房间实例
     *
     * @param roomConfigId roomConfig的Id
     * @return 房间实例
     */
    @Override
    public BaseRoom createRoomInstanceByRoomConfigId(Long roomConfigId) throws Exception {
        if (null == roomConfigId) {
            return null;
        }
        RoomConfig roomConfig = roomConfigService.findOne(roomConfigId);
        if (null == roomConfig) {
            return null;
        }
        AbstractRoomBeanDefinitionReader roomBeanDefinitionReader = new XmlRoomBeanDefinitionReader(new DefaultResourceLoader());
        roomBeanDefinitionReader.loadRoomBeanDefinitions(roomConfig.getRoomConfigContent());
        AbstractRoomBeanFactory roomBeanFactory = new AutowireCapableRoomBeanFactory();
        roomBeanFactory.setRegisterMap(roomBeanDefinitionReader.getRoomBeanDefinition());
        BaseRoom room = (BaseRoom) roomBeanFactory.getRoomBean(roomConfig.getRoomBeanName());
        String roomId = IdUtils.getRandomIdString();
        room.setRoomId(roomId);
        room.setRoomConfigId(roomConfigId);
        room.setRoomType(roomConfig.getRoomConfigType());
        add(room);
        return room;
    }

    /**
     * 添加
     *
     * @param baseRoom 实体
     * @return 是否添加成功
     */
    @Override
    public boolean add(BaseRoom baseRoom) {
        roomDao.addBaseRoom(baseRoom);
        return true;
    }

    /**
     * 根据id字段对其它非空字段进行修改
     *
     * @param baseRoom 实体
     * @return 是否修改成功
     */
    @Override
    public boolean update(BaseRoom baseRoom) {
        String roomId = baseRoom.getRoomId();
        BaseRoom room = roomDao.getBaseRoom(roomId);
        FieldUtils.copyFileds(baseRoom, room);
        roomDao.addBaseRoom(room);
        return true;
    }

    /**
     * 根据id删除
     *
     * @param id 实体的id
     * @return 是否删除成功
     */
    @Override
    public boolean delete(String id) {
        roomDao.deleteBaseRoom(id);
        return true;
    }

    /**
     * 根据id获取实体
     *
     * @param id 实体
     * @return 查找实体
     */
    @Override
    public BaseRoom findOne(String id) {
        return roomDao.getBaseRoom(id);
    }

    /**
     * 获取所有对象
     *
     * @return 对象列表
     */
    @Override
    public List<BaseRoom> findAll() {
        return roomDao.findAll();
    }
}
