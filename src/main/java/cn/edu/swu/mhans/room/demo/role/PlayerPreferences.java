package cn.edu.swu.mhans.room.demo.role;

import lombok.Data;

import java.io.Serializable;
import java.util.Map;

/**
 * 玩家偏好
 *
 * @author ZhanJingbo
 * @version 1.0.0
 * Created on 2017/9/13
 */
@Data
public class PlayerPreferences implements Serializable {
    private String name;
    private Map<String, Double> preferenceWeight;
    private Map<String, Double> optionValues;
}
