package cn.edu.swu.mhans.room.demo.issue;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 离散型Issue的实现
 *
 * @author ZhanJingbo
 * @version 1.0.0
 * Created on 2017/9/12
 */
@Data
public class DiscreteIssue implements Serializable {

    /**
     * 议题名称
     */
    private String name;
    /**
     * 议题描述
     */
    private String description;

    /**
     * 议题的选项
     */
    private List<Option> options;

}
