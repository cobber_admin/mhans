package cn.edu.swu.mhans.room.demo.issue;


import lombok.Data;

import java.io.Serializable;

/**
 * @author ZhanJingbo
 * @version 1.0.0
 * Created on 2018/3/28
 */
@Data
public class Option implements Serializable {
    /**
     * 选项名称
     */
    private String name;
    /**
     * 选项描述
     */
    private String description;

}
