package cn.edu.swu.mhans.room.bean.io;

import java.net.URL;

/**
 * @author ZhanJingbo
 * @version 1.0.0
 * Created on 2018/3/24
 */
public class DefaultResourceLoader implements ResourceLoader {


    private static final String PATH_SUFFIX = ".xml";

    /**
     * 通过参数获取Resource对象
     *
     * @param resourceString resource对象的字符串
     * @return Resource对象
     */
    @Override
    public Resource getResource(String resourceString) {

        if (resourceString.endsWith(PATH_SUFFIX)) {
            return getResourceWithLocation(resourceString);
        }

        return getResourceWithXMLString(resourceString);
    }

    private Resource getResourceWithLocation(String location) {
        URL resource = this.getClass().getClassLoader().getResource(location);
        return new UrlResource(resource);
    }

    private Resource getResourceWithXMLString(String xmlString) {
        return new StringResource(xmlString);
    }
}
