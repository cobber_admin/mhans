package cn.edu.swu.mhans.web.controller;

import cn.edu.swu.mhans.room.service.RoomService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;

/**
 * @author ZhanJingbo
 * @version 1.0.0
 * Created on 2018/4/4
 */
@Controller
public class IndexController {

    @Resource
    private RoomService roomService;

    @RequestMapping("/")
    public ModelAndView index() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("index");
        mav.addObject("rooms", roomService.findAll());
        return mav;
    }
}
