package cn.edu.swu.mhans.web.common.model;

import lombok.Data;


import java.util.Date;

/**
 * @author ZhanJingbo
 * @version 1.0.0
 * Created on 2018/5/3
 */
@Data
public class SystemAttribute {
    /**
     * id
     */
    private Long id;
    /**
     * 参数的键
     */
    private String k;
    /**
     * 参数的值
     */
    private String v;

    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 修改时间
     */
    private Date modifyTime;
}
