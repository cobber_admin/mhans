package cn.edu.swu.mhans.web.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author ZhanJingbo
 * @version 1.0.0
 * Created on 2018/4/24
 */
@Controller
public class ErrorController {

    @RequestMapping("/404")
    public String pageNotFound() {
        return "error/404";
    }

    @RequestMapping("/403")
    public String accessDefine() {
        return "error/403";
    }

    @RequestMapping("/500")
    public String serverError() {
        return "error/500";
    }
}
