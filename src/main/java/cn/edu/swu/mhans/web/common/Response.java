package cn.edu.swu.mhans.web.common;

import lombok.Data;

/**
 * @author ZhanJingbo
 * @version 1.0.0
 * Created on 2017/10/13
 */
@Data
public class Response<T> {
    private boolean success = false;
    private Integer status;
    private String message;
    private T data;

    public Response() {
    }

    public Response(T data) {
        this.data = data;
        this.status = 200;
        success = true;
    }

}
