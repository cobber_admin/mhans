package cn.edu.swu.mhans.web.controller;

import cn.edu.swu.mhans.room.model.RoomConfig;
import cn.edu.swu.mhans.room.service.RoomConfigService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;

/**
 * @author ZhanJingbo
 * @version 1.0.0
 * Created on 2018/4/10
 */
@Controller
@RequestMapping("/admin/roomConfig")
@Slf4j
public class RoomConfigController {

    @Resource
    private RoomConfigService roomConfigService;

    @RequestMapping(value = {"/", "/index", ""})
    public ModelAndView roomConfigIndex() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("/admin/roomConfig/index");
        mav.addObject("roomConfigList", roomConfigService.findAll());
        return mav;
    }

    @RequestMapping(value = "/save", method = RequestMethod.GET)
    public ModelAndView savePage(@RequestParam(required = false) Long id) {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("/admin/roomConfig/save");
        if (null != id) {
            mav.addObject("roomConfig", roomConfigService.findOne(id));
        } else {
            mav.addObject("roomConfig", new RoomConfig());
        }
        return mav;
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String saveProcessHandle(RoomConfig roomConfig) {
        if (null == roomConfig.getId()) {
            roomConfigService.add(roomConfig);
        } else {
            roomConfigService.update(roomConfig);
        }

        return "redirect:/admin/roomConfig/index";
    }

    @RequestMapping("/delete")
    public String deleteRoomConfig(Long id) {
        if (null != id) {
            roomConfigService.delete(id);
        }
        return "redirect:/admin/roomConfig/index";
    }


}
