package cn.edu.swu.mhans.web.controller.admin;

import cn.edu.swu.mhans.room.service.RoomService;
import cn.edu.swu.mhans.room.share.base.BaseRoom;
import cn.edu.swu.mhans.web.common.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * @author ZhanJingbo
 * @version 1.0.0
 * Created on 2018/4/10
 */
@Controller
@RequestMapping("/admin/room")
@Slf4j
public class AdminRoomController {

    @Resource
    private RoomService roomService;

    @RequestMapping("/createRoomInstance")
    @ResponseBody
    public Response createRoomInstance(Long roomConfigId) throws Exception {
        BaseRoom room = roomService.createRoomInstanceByRoomConfigId(roomConfigId);
        return new Response(room);
    }
}
