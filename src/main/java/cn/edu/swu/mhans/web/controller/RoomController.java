package cn.edu.swu.mhans.web.controller;

import cn.edu.swu.mhans.room.constant.RoomStatus;
import cn.edu.swu.mhans.room.service.RoomService;
import cn.edu.swu.mhans.room.share.base.BaseRoom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.websocket.server.PathParam;

/**
 * @author ZhanJingbo
 * @version 1.0.0
 * Created on 2018/4/12
 */
@Controller
@RequestMapping("/room")
public class RoomController {

    @Autowired
    private RoomService roomService;

    @RequestMapping("/into/{roomId}")
    public ModelAndView intoGameRoom(@PathVariable("roomId") String roomId) {
        ModelAndView mav = new ModelAndView();
        BaseRoom room = roomService.findOne(roomId);
        if (null == room || room.getRoomStatus() != RoomStatus.READY.getValue()) {
            mav.setViewName("redirect:/");
            return mav;
        }
        mav.addObject("room", room);
        mav.setViewName("room");
        return mav;
    }
}
