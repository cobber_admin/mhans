package cn.edu.swu.mhans.web.common;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 统一异常处理
 *
 * @author ZhanJingbo
 * @version 1.0.0
 * Created on 2018/5/21
 */
@ControllerAdvice
public class GlobalExceptionHandle {

    private final static Integer ERROR_STATUS = -1;

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public Response handleRuntimeException(Exception exception) {
        Response response = new Response();
        response.setSuccess(false);
        response.setStatus(ERROR_STATUS);
        response.setMessage(exception.getMessage());
        return response;
    }
}
