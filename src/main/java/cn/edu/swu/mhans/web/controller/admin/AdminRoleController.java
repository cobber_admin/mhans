package cn.edu.swu.mhans.web.controller.admin;

import cn.edu.swu.mhans.user.model.SystemRole;
import cn.edu.swu.mhans.user.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * @author ZhanJingbo
 * @version 1.0.0
 * Created on 2018/4/25
 */
@Controller
@RequestMapping("/admin/role")
public class AdminRoleController {

    @Autowired
    private RoleService roleService;

    @RequestMapping("/listJson")
    @ResponseBody
    public List<SystemRole> getAllRoles() {
        return roleService.findAll();
    }

    @RequestMapping(value = {"/", "/index"})
    public ModelAndView getAllRolesPage() {
        ModelAndView mav = new ModelAndView();
        mav.addObject("roleList", roleService.findAll());
        mav.setViewName("/admin/role/index");
        return mav;
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addRole(SystemRole systemRole) {
        roleService.add(systemRole);
        return redirectToRoleIndex();
    }

    @RequestMapping(value = "/delete")
    public String deleteRole(Long roleId) {
        roleService.delete(roleId);
        return redirectToRoleIndex();
    }

    private String redirectToRoleIndex() {
        return "redirect:/admin/role/";
    }
}
