package cn.edu.swu.mhans.web.controller.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author ZhanJingbo
 * @version 1.0.0
 * Created on 2018/4/10
 */
@Controller
@RequestMapping("/admin")
public class AdminController {

    @RequestMapping(value = {"/", "/index",""})
    public String index() {
        return "admin/index";
    }
}
