package cn.edu.swu.mhans.web.controller;

import cn.edu.swu.mhans.game.NegotiationResultDto;
import cn.edu.swu.mhans.game.model.NegotiationResult;
import cn.edu.swu.mhans.game.service.NegotiationResultService;
import cn.edu.swu.mhans.user.model.SystemUser;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author ZhanJingbo
 * @version 1.0.0
 * Created on 2018/5/4
 */
@Controller
@RequestMapping("/negotiationResult")
@Slf4j
public class NegotiationResultController {

    @Autowired
    private NegotiationResultService negotiationResultService;

    @RequestMapping(value = {"/", "/index"})
    public ModelAndView getNegotiationResult(HttpSession session) {
        SystemUser currentUser = (SystemUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        ModelAndView mav = new ModelAndView();
        mav.setViewName("negotiationResult");
        List<NegotiationResult> negotiationResultList = negotiationResultService.findNegotiationResultByUserId(currentUser.getId());
        List<NegotiationResultDto> negotiationResultDtoList = Lists.newArrayList();
        for (NegotiationResult negotiationResult : negotiationResultList) {
            negotiationResultDtoList.add(NegotiationResultDto.convertFromNegotiationResult(negotiationResult));
        }
        mav.addObject("negotiationResults", negotiationResultDtoList);
        return mav;
    }
}
