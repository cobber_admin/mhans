package cn.edu.swu.mhans.game.service.impl;

import cn.edu.swu.mhans.common.util.FieldUtils;
import cn.edu.swu.mhans.game.dao.ActionLogMapper;
import cn.edu.swu.mhans.game.model.ActionLog;
import cn.edu.swu.mhans.game.service.ActionLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author ZhanJingbo
 * @version 1.0.0
 * Created on 2018/5/3
 */
@Service
public class ActionLogServiceImpl implements ActionLogService {

    @Autowired
    private ActionLogMapper actionLogMapper;

    /**
     * 添加
     *
     * @param actionLog 实体
     * @return 是否添加成功
     */
    @Override
    public boolean add(ActionLog actionLog) {
        if (null == actionLog) {
            return false;
        }
        return actionLogMapper.insert(actionLog) != 0;
    }

    /**
     * 根据id字段对其它非空字段进行修改
     *
     * @param actionLog 实体
     * @return 是否修改成功
     */
    @Override
    public boolean update(ActionLog actionLog) {
        ActionLog result = actionLogMapper.selectByPrimaryKey(actionLog.getId());
        FieldUtils.copyFileds(actionLog, result);
        return actionLogMapper.updateByPrimaryKey(result) != 0;
    }

    /**
     * 根据id删除
     *
     * @param id 实体的id
     * @return 是否删除成功
     */
    @Override
    public boolean delete(Long id) {
        return actionLogMapper.deleteByPrimaryKey(id) != 0;
    }

    /**
     * 根据id获取实体
     *
     * @param id 实体
     * @return 查找实体
     */
    @Override
    public ActionLog findOne(Long id) {
        return actionLogMapper.selectByPrimaryKey(id);
    }

    /**
     * 获取所有对象
     *
     * @return 对象列表
     */
    @Override
    public List<ActionLog> findAll() {
        return actionLogMapper.selectAll();
    }
}
