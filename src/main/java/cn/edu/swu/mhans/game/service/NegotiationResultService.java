package cn.edu.swu.mhans.game.service;

import cn.edu.swu.mhans.common.service.BaseService;
import cn.edu.swu.mhans.game.model.NegotiationResult;

import java.util.List;

/**
 * @author ZhanJingbo
 * @version 1.0.0
 * Created on 2018/5/3
 */
public interface NegotiationResultService extends BaseService<NegotiationResult, Long> {

    /**
     * 检索某用户的协商结果记录
     * @param userId 用户id
     * @return 协商结果
     */
    List<NegotiationResult> findNegotiationResultByUserId(Long userId);
}
