package cn.edu.swu.mhans.game.model;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
@Data
public class NegotiationResult implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * id
     */
    private Long id;
    /**
     * 配置文件id
     */
    private Long roomConfigId;
    /**
     * 房间名称
     */
    private String roomName;
    /**
     * 用户id
     */
    private String userId;
    /**
     * 用户类别
     */
    private Integer userType;
    /**
     * 协商结果
     */
    private String negotiationResult;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 修改时间
     */
    private Date modifyTime;
    /**
     * 扩展字段
     */
    private String ext;
}