package cn.edu.swu.mhans.game.dao;

import cn.edu.swu.mhans.game.model.ActionLog;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface ActionLogMapper {
    /**
     * 通过主键删除
     */
    int deleteByPrimaryKey(Long id);

    /**
     * 添加
     */
    int insert(ActionLog record);

    /**
     * 通过主键查询
     */
    ActionLog selectByPrimaryKey(Long id);

    /**
     * 查出所有
     */
    List<ActionLog> selectAll();

    /**
     * 通过主键更新
     */
    int updateByPrimaryKey(ActionLog record);
}