package cn.edu.swu.mhans.game.service;

import cn.edu.swu.mhans.common.service.BaseService;
import cn.edu.swu.mhans.game.model.ActionLog;

/**
 * 操作日志记录服务
 *
 * @author ZhanJingbo
 * @version 1.0.0
 * Created on 2018/5/3
 */
public interface ActionLogService extends BaseService<ActionLog,Long> {
}
