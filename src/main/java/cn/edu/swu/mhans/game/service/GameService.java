package cn.edu.swu.mhans.game.service;


import javax.websocket.Session;
import java.io.IOException;

/**
 * 游戏WebSocket连接所调用的业务逻辑接口ßß
 *
 * @author ZhanJingbo
 * @version 1.0.0
 * Created on 2017/11/11
 */
public interface GameService {

    /**
     * 用户连入WebSocket房间所进行的操作
     *
     * @param roomId   房间Id
     * @param playerId 玩家Id
     * @param session  WebSocket的Session
     */
    void onConnectionOpen(String roomId, Long playerId, Session session);

    /**
     * 用户发送消息的处理接口
     *
     * @param jsonCommand 用户发送的消息，应该是Command对象的Json串
     */
    void onMessage(String jsonCommand) throws IOException;

    /**
     * 用户断开连接所执行的操作
     *
     * @param roomId   房间id
     * @param playerId 玩家id
     */
    void onConnectionClose(String roomId, Long playerId);

    /**
     * 连接过程中的异常处理
     *
     * @param error 发生的异常
     */
    void onConnectionError(Throwable error);
}
