package cn.edu.swu.mhans.game.model;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class ActionLog implements Serializable {
    private static final long serialVersionUID = -7193681420665295414L;
    /**
     * Log主键
     */
    private Long id;

    /**
     * 用户id
     */
    private String userId;

    /**
     * 用户类别
     */
    private Integer userType;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改时间
     */
    private Date modifyTime;

    /**
     * 操作-json格式
     */
    private String action;

}