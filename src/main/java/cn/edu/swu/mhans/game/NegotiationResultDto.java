package cn.edu.swu.mhans.game;

import cn.edu.swu.mhans.common.util.FieldUtils;
import cn.edu.swu.mhans.game.model.NegotiationResult;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;


import java.io.IOException;
import java.util.Date;
import java.util.Map;

/**
 * @author ZhanJingbo
 * @version 1.0.0
 * Created on 2018/5/4
 */
@Data
public class NegotiationResultDto {
    private Long id;


    /**
     * 配置文件id
     */
    private Long roomConfigId;
    /**
     * 房间名称
     */
    private String roomName;
    /**
     * 协商结果
     */
    private String negotiationResult;

    /**
     * 创建时间
     */
    private Date createTime;


    private Map<String, Object> resultMap;

    public static NegotiationResultDto convertFromNegotiationResult(NegotiationResult negotiationResult) {
        try {
            NegotiationResultDto dto = new NegotiationResultDto();
            FieldUtils.convert(negotiationResult, dto);
            ObjectMapper mapper = new ObjectMapper();
            Map<String, Object> resultMap = mapper.readValue(negotiationResult.getNegotiationResult(), Map.class);
            dto.setResultMap(resultMap);
            return dto;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
