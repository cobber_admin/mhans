package cn.edu.swu.mhans.game.service.impl;

import cn.edu.swu.mhans.common.util.FieldUtils;
import cn.edu.swu.mhans.game.dao.NegotiationResultMapper;
import cn.edu.swu.mhans.game.model.NegotiationResult;
import cn.edu.swu.mhans.game.service.NegotiationResultService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author ZhanJingbo
 * @version 1.0.0
 * Created on 2018/5/3
 */
@Service
public class NegotiationResultServiceImpl implements NegotiationResultService {

    @Autowired
    private NegotiationResultMapper negotiationResultMapper;

    /**
     * 添加
     *
     * @param negotiationResult 实体
     * @return 是否添加成功
     */
    @Override
    public boolean add(NegotiationResult negotiationResult) {
        if (null == negotiationResult) {
            return false;
        }
        return negotiationResultMapper.insert(negotiationResult) != 0;
    }

    /**
     * 根据id字段对其它非空字段进行修改
     *
     * @param negotiationResult 实体
     * @return 是否修改成功
     */
    @Override
    public boolean update(NegotiationResult negotiationResult) {
        NegotiationResult result = negotiationResultMapper.selectByPrimaryKey(negotiationResult.getId());
        FieldUtils.copyFileds(negotiationResult, result);
        return negotiationResultMapper.updateByPrimaryKey(result) != 0;
    }

    /**
     * 根据id删除
     *
     * @param id 实体的id
     * @return 是否删除成功
     */
    @Override
    public boolean delete(Long id) {
        return negotiationResultMapper.deleteByPrimaryKey(id) != 0;
    }

    /**
     * 根据id获取实体
     *
     * @param id 实体
     * @return 查找实体
     */
    @Override
    public NegotiationResult findOne(Long id) {
        return negotiationResultMapper.selectByPrimaryKey(id);
    }

    /**
     * 获取所有对象
     *
     * @return 对象列表
     */
    @Override
    public List<NegotiationResult> findAll() {
        return negotiationResultMapper.selectAll();
    }

    /**
     * 检索某用户的协商结果记录
     *
     * @param userId 用户id
     * @return 协商结果
     */
    @Override
    public List<NegotiationResult> findNegotiationResultByUserId(Long userId) {
        return negotiationResultMapper.selectByUserId(userId.toString());
    }
}
