package cn.edu.swu.mhans.game.dao;

import cn.edu.swu.mhans.game.model.NegotiationResult;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface NegotiationResultMapper {
    int deleteByPrimaryKey(Long id);

    int insert(NegotiationResult record);

    NegotiationResult selectByPrimaryKey(Long id);

    List<NegotiationResult> selectAll();

    int updateByPrimaryKey(NegotiationResult record);

    List<NegotiationResult> selectByUserId(String userId);
}