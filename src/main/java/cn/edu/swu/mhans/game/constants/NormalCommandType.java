package cn.edu.swu.mhans.game.constants;

/**
 * @author ZhanJingbo
 * @version 1.0.0
 * Created on 2018/5/1
 */
public enum NormalCommandType {
    /**
     * 聊天信息
     */
    CHAT_MESSAGE(0, "ChatMessage"),
    /**
     * 游戏开始
     */
    GAME_START(1, "GameStart"),
    /**
     * 角色分配，发送至前端
     */
    ALLOC_ROLE(2, "AllocRole"),
    /**
     * 选举出价人
     */
    CHOOSE_MC(3, "ChooseMc"),
    /**
     * mc出价
     */
    MC_OFFER(4, "McOffer"),
    /**
     * 表决
     */
    VOTE(5, "vote"),
    /**
     * 玩家表决结果
     */
    PLAYER_VOTE(6, "PlayerVote"),
    /**
     * 游戏结束
     */
    GAME_OVER(7, "GameOver"),
    /**
     * 游戏异常中断结束
     */
    BREAK_OFF(8, "BreakOff"),
    /**
     * 玩家进入房间
     */
    INTO_ROOM(9, "IntoRoom");

    NormalCommandType(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    private Integer value;

    private String desc;

    public Integer getValue() {
        return value;
    }

    public String getDesc() {
        return desc;
    }

    public static NormalCommandType getCommandType(Integer type) {
        for (NormalCommandType commandType : NormalCommandType.values()) {
            if (commandType.value.equals(type)) {
                return commandType;
            }
        }
        return null;
    }

}
