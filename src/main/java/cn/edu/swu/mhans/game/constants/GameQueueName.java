package cn.edu.swu.mhans.game.constants;


/**
 * @author ZhanJingbo
 * @version 1.0.0
 * Created on 2018/5/22
 */
public enum GameQueueName {
    NORMAL_ROOM("normal", "normalGame");

    private String roomType;

    private String queueName;

    GameQueueName(String roomType, String queueName) {
        this.roomType = roomType;
        this.queueName = queueName;
    }

    public String getRoomType() {
        return roomType;
    }

    public String getQueueName() {
        return queueName;
    }

    /**
     * 根据房间类型 返回对应的队列名称
     *
     * @param roomType 根据房间类型返回队列名称
     * @return 队列名称
     */
    public static String getQueueNameByRoomType(String roomType) {
        for (GameQueueName gameQueueName : GameQueueName.values()) {
            if (gameQueueName.getRoomType().equals(roomType)) {
                return gameQueueName.getQueueName();
            }
        }
        return null;
    }
}
