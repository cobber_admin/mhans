package cn.edu.swu.mhans;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;

/**
 * @author ZhanJingbo
 */
@SpringBootApplication
@EnableWebSocket
@EnableWebSecurity
//@MapperScan(basePackages = "cn.edu.swu.mhans.***.dao")
public class MhansApplication {
    public static void main(String[] args) {
        SpringApplication.run(MhansApplication.class, args);
    }
}
