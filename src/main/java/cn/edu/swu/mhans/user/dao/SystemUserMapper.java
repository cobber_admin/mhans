package cn.edu.swu.mhans.user.dao;

import cn.edu.swu.mhans.user.model.SystemUser;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface SystemUserMapper {
    int deleteByPrimaryKey(Long id);

    Long insert(SystemUser record);

    SystemUser selectByPrimaryKey(Long id);

    List<SystemUser> selectAll();

    int updateByPrimaryKey(SystemUser record);

    SystemUser selectByUsername(String username);
}