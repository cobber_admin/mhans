package cn.edu.swu.mhans.user.model;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class SystemRole implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String roleName;

    private Date createTime;

    private Date modifyTime;

}