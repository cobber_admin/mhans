package cn.edu.swu.mhans.user.dao;

import cn.edu.swu.mhans.user.model.SystemUserRoles;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface SystemUserRolesMapper {
    int deleteByPrimaryKey(Long id);

    int insert(SystemUserRoles record);

    SystemUserRoles selectByPrimaryKey(Long id);

    List<SystemUserRoles> selectAll();

    int updateByPrimaryKey(SystemUserRoles record);

    List<SystemUserRoles> selectByUserId(Long userId);

    List<SystemUserRoles> selectByRoleId(Long roleId);
}