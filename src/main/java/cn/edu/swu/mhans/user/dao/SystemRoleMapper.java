package cn.edu.swu.mhans.user.dao;

import cn.edu.swu.mhans.user.model.SystemRole;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
@Mapper
public interface SystemRoleMapper {
    int deleteByPrimaryKey(Long id);

    int insert(SystemRole record);

    SystemRole selectByPrimaryKey(Long id);

    List<SystemRole> selectAll();

    int updateByPrimaryKey(SystemRole record);

    List<SystemRole> findAllByIdIn(@Param("ids") List<Long> ids);
}