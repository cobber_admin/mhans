package cn.edu.swu.mhans.user.service;

import cn.edu.swu.mhans.common.service.BaseService;
import cn.edu.swu.mhans.user.model.SystemRole;

import java.util.List;

/**
 * @author ZhanJingbo
 * @version 1.0.0
 * Created on 2018/4/25
 */
public interface RoleService extends BaseService<SystemRole, Long> {
    List<SystemRole> findAllByIdIn(List<Long> ids);
}
