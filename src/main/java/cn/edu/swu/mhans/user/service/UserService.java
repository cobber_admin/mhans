package cn.edu.swu.mhans.user.service;

import cn.edu.swu.mhans.common.service.BaseService;
import cn.edu.swu.mhans.user.model.SystemUser;
import org.springframework.security.core.userdetails.UserDetailsService;

import javax.websocket.Session;
import java.util.List;

/**
 * @author ZhanJingbo
 * @version 1.0.0
 * Created on 2018/4/23
 */
public interface UserService extends UserDetailsService, BaseService<SystemUser, Long> {

    /**
     * 用户注册
     *
     * @param systemUser 待注册用户
     * @return 返回是否注册成功
     */
    boolean register(SystemUser systemUser);

    void saveUserWebSocketSession(String sessionKey, Session session);

    Session getUserWebSocketSession(String sessionKey);
}
