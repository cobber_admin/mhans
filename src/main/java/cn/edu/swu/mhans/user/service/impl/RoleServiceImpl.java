package cn.edu.swu.mhans.user.service.impl;

import cn.edu.swu.mhans.common.util.FieldUtils;
import cn.edu.swu.mhans.user.dao.SystemRoleMapper;
import cn.edu.swu.mhans.user.model.SystemRole;
import cn.edu.swu.mhans.user.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @author ZhanJingbo
 * @version 1.0.0
 * Created on 2018/4/25
 */
@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private SystemRoleMapper systemRoleMapper;

    /**
     * 添加
     *
     * @param systemRole 实体
     * @return 是否添加成功
     */
    @Override
    public boolean add(SystemRole systemRole) {
        Date now = new Date();
        systemRole.setModifyTime(now);
        systemRole.setModifyTime(now);
        return systemRoleMapper.insert(systemRole) != 0;
    }

    /**
     * 根据id字段对其它非空字段进行修改
     *
     * @param systemRole 实体
     * @return 是否修改成功
     */
    @Override
    public boolean update(SystemRole systemRole) {
        SystemRole entity = systemRoleMapper.selectByPrimaryKey(systemRole.getId());
        FieldUtils.copyFileds(systemRole, entity);
        entity.setModifyTime(new Date());
        return systemRoleMapper.updateByPrimaryKey(entity) != 0;
    }

    /**
     * 根据id删除
     *
     * @param id 实体的id
     * @return 是否删除成功
     */
    @Override
    public boolean delete(Long id) {
        return systemRoleMapper.deleteByPrimaryKey(id) != 0;
    }

    /**
     * 根据id获取实体
     *
     * @param id 实体
     * @return 查找实体
     */
    @Override
    public SystemRole findOne(Long id) {
        return systemRoleMapper.selectByPrimaryKey(id);
    }

    /**
     * 获取所有对象
     *
     * @return 对象列表
     */
    @Override
    public List<SystemRole> findAll() {
        return systemRoleMapper.selectAll();
    }

    @Override
    public List<SystemRole> findAllByIdIn(List<Long> ids) {
        return systemRoleMapper.findAllByIdIn(ids);
    }
}
