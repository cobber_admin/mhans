package cn.edu.swu.mhans.user.model;

import lombok.Data;

import java.io.Serializable;
import java.util.Objects;

@Data
public class SystemUserRoles implements Serializable {
    private Long id;

    private Long systemUserId;

    private Long rolesId;

    private static final long serialVersionUID = 1L;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SystemUserRoles that = (SystemUserRoles) o;
        return Objects.equals(systemUserId, that.systemUserId) &&
                Objects.equals(rolesId, that.rolesId);
    }

    @Override
    public int hashCode() {

        return Objects.hash(systemUserId, rolesId);
    }

    public SystemUserRoles(Long systemUserId, Long rolesId) {
        this.systemUserId = systemUserId;
        this.rolesId = rolesId;
    }

    public SystemUserRoles() {
    }
}