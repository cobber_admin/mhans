package cn.edu.swu.mhans.user.security;

import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * Security User 密码解析器
 *
 * @author ZhanJingbo
 * @version 1.0.0
 * Created on 2018/5/10
 */
public class UserPasswordEncoder implements PasswordEncoder {
    @Override
    public String encode(CharSequence charSequence) {
        return charSequence.toString();
    }

    @Override
    public boolean matches(CharSequence charSequence, String s) {
        return s.equals(charSequence.toString());
    }
}
