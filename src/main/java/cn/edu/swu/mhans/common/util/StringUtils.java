package cn.edu.swu.mhans.common.util;

/**
 * 字符串处理工具类
 *
 * @author ZhanJingbo
 * @version 1.0.0
 * Created on 2018/3/27
 */
public class StringUtils {

    /**
     * 首字母小写
     *
     * @param str 源串
     * @return 目标串
     */
    public static String getLowerCaseStringForFirstChar(String str) {
        StringBuilder builder = new StringBuilder();
        builder.append(str.toLowerCase().charAt(0));
        builder.append(str.substring(1));
        return builder.toString();
    }

    /**
     * 首字母大写
     *
     * @param str 源串
     * @return 目标串
     */
    public static String getUpperCaseStringForFirstChar(String str) {
        StringBuilder builder = new StringBuilder();
        builder.append(str.toUpperCase().charAt(0));
        builder.append(str.substring(1));
        return builder.toString();
    }

}
