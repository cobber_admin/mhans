package cn.edu.swu.mhans.common.service;

import java.util.List;

/**
 * @author ZhanJingbo
 * @version 1.0.0
 * Created on 2018/4/11
 */
public interface BaseService<T, K> {

    /**
     * 添加
     *
     * @param t 实体
     * @return 是否添加成功
     */
    boolean add(T t);

    /**
     * 根据id字段对其它非空字段进行修改
     *
     * @param t 实体
     * @return 是否修改成功
     */
    boolean update(T t);

    /**
     * 根据id删除
     *
     * @param id 实体的id
     * @return 是否删除成功
     */
    boolean delete(K id);

    /**
     * 根据id获取实体
     *
     * @param id 实体
     * @return 查找实体
     */
    T findOne(K id);

    /**
     * 获取所有对象
     *
     * @return 对象列表
     */
    List<T> findAll();

}
