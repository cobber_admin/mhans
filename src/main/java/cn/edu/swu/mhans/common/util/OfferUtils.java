package cn.edu.swu.mhans.common.util;

import cn.edu.swu.mhans.room.demo.role.PlayerPreferences;

import java.util.Map;

/**
 * @author ZhanJingbo
 * @version 1.0.0
 * Created on 2017/11/18
 */
public class OfferUtils {

    /**
     * 根据角色和指定的出价计算收益
     *
     * @param offerData         玩家的出价
     * @param playerPreferences 玩家的偏好
     * @return
     */
    public static double calcScore(Map<String, Object> offerData, PlayerPreferences playerPreferences) {
        double score = 0;
        for (String issueName : offerData.keySet()) {
            String issueSelectOption = offerData.get(issueName).toString();
            double selectValue = playerPreferences.getOptionValues().get(issueSelectOption);
            score += selectValue * playerPreferences.getPreferenceWeight().get(issueName);
        }
        return score;
    }
}
