package cn.edu.swu.mhans.common.util;

import java.util.UUID;

/**
 * ID相关的工具类
 *
 * @author ZhanJingbo
 * @version 1.0.0
 * Created on 2017/10/27
 */
public class IdUtils {

    /**
     * 生成一个随机的UUID字符串
     *
     * @return UUID 字符串
     */
    public static String getRandomIdString() {
        return UUID.randomUUID().toString();
    }

}
