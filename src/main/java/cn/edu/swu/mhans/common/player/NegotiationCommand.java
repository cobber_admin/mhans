package cn.edu.swu.mhans.common.player;

import lombok.Data;

import java.util.Map;

/**
 * 协商传输数据
 *
 * @author ZhanJingbo
 * @version 1.0.0
 * Created on 2018/3/16
 */
@Data
public class NegotiationCommand {

    /**
     * 命令的类型
     */
    private Integer commandType;
    /**
     * 来源协商者id
     */
    private String fromNegotiationPlayerId;
    /**
     * 协商房间id
     */
    private String roomId;

    /**
     * 数据正文
     */
    private Map<String, Object> data;


    public NegotiationCommand(){

    }

    public NegotiationCommand(Integer commandType, String roomId) {
        this.commandType = commandType;
        this.roomId = roomId;
    }
}
