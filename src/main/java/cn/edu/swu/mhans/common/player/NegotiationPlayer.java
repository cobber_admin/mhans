package cn.edu.swu.mhans.common.player;

import cn.edu.swu.mhans.room.demo.role.PlayerPreferences;

import java.io.Serializable;

/**
 * 协商参与者统一接口
 *
 * @author ZhanJingbo
 * @version 1.0.0
 * Created on 2018/3/16
 */
public interface NegotiationPlayer extends Serializable {

    /**
     * 获取协商参与者的游戏id
     *
     * @return 协商参与者的游戏id
     */
    String getNegotiationPlayerId();

    /**
     * 获取协商参与者的游戏昵称
     *
     * @return 协商参与者的游戏昵称
     */
    String getNegotiationPlayerName();

    /**
     * 返回协商参与者的类型
     *
     * @return 自然人返回1，Agent返回0
     */
    Integer getNegotiationPlayerType();

    /**
     * 处理传输来的数据
     *
     * @param negotiationCommand 待处理的数据
     */
    void handleNegotiationCommand(NegotiationCommand negotiationCommand);

    /**
     * 设置玩家偏好
     *
     * @param playerPreferences 玩家偏好
     */
    void setPlayerPreferences(PlayerPreferences playerPreferences);

    /**
     * 获取玩家偏好
     *
     * @return 玩家偏好
     */
    PlayerPreferences getPlayerPreference();
}
