package cn.edu.swu.mhans.common.player;

import cn.edu.swu.mhans.room.demo.role.PlayerPreferences;
import lombok.Data;

/**
 * @author ZhanJingbo
 * @version 1.0.0
 * Created on 2018/3/16
 */
@Data
public abstract class AbstractNegotiationAgentPlayer implements NegotiationPlayer {

    private String agentId;
    private String agentName;
    /**
     * 玩家偏好
     */
    private PlayerPreferences playerPreferences;


    /**
     * 获取协商参与者的游戏id
     *
     * @return 协商参与者的游戏id
     */
    @Override
    public String getNegotiationPlayerId() {
        return this.agentId;
    }

    /**
     * 获取协商参与者的游戏昵称
     *
     * @return 协商参与者的游戏昵称
     */
    @Override
    public String getNegotiationPlayerName() {
        return this.agentName;
    }

    /**
     * 返回协商参与者的类型
     *
     * @return 自然人返回1，Agent返回0
     */
    @Override
    public Integer getNegotiationPlayerType() {
        return 0;
    }

    /**
     * 处理传输来的数据
     *
     * @param negotiationCommand 待处理的数据
     */
    @Override
    public abstract void handleNegotiationCommand(NegotiationCommand negotiationCommand);

    @Override
    public void setPlayerPreferences(PlayerPreferences playerPreferences) {
        this.playerPreferences = playerPreferences;
    }

    @Override
    public PlayerPreferences getPlayerPreference() {
        return this.playerPreferences;
    }
}
