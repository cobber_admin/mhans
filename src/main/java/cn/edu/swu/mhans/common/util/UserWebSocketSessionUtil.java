package cn.edu.swu.mhans.common.util;

/**
 * @author ZhanJingbo
 * @version 1.0.0
 * Created on 2018/4/27
 */
public class UserWebSocketSessionUtil {

    private static final String SESSION_KEY_FORMAT = "SESSION_%s_%s";

    public static String getUserWebSocketSessionKey(String roomId, Long userId) {
        return String.format(SESSION_KEY_FORMAT, roomId, userId.toString());
    }
}
