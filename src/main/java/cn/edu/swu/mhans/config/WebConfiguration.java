package cn.edu.swu.mhans.config;


import org.springframework.boot.web.embedded.tomcat.TomcatConnectorCustomizer;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.server.ErrorPage;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * @author ZhanJingbo
 * @version 1.0.0
 * Created on 2018/4/23
 */
@Configuration
public class WebConfiguration implements WebMvcConfigurer {


    @Bean
    public ConfigurableServletWebServerFactory containerCustomizer() {


        TomcatServletWebServerFactory factory = new TomcatServletWebServerFactory();
        ErrorPage error401Page = new ErrorPage(HttpStatus.FORBIDDEN, "/403");
        ErrorPage error404Page = new ErrorPage(HttpStatus.NOT_FOUND, "/404");
        ErrorPage error500Page = new ErrorPage(HttpStatus.INTERNAL_SERVER_ERROR, "/500");
        factory.addErrorPages(error401Page, error404Page, error500Page);
        return factory;

    }

}
