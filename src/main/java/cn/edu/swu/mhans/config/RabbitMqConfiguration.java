package cn.edu.swu.mhans.config;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author ZhanJingbo
 * @version 1.0.0
 * Created on 2018/4/28
 */
@Configuration
public class RabbitMqConfiguration {
    @Bean
    public Queue normalGameQueue() {
        return new Queue("normalGame");
    }
}
