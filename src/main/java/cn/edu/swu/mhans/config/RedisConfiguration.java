package cn.edu.swu.mhans.config;

import cn.edu.swu.mhans.common.redis.RedisObjectSerializer;
import cn.edu.swu.mhans.room.share.base.BaseRoom;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;

/**
 * Redis 相关配置
 *
 * @author ZhanJingbo
 * @version 1.0.0
 * Created on 2018/5/9
 */
@Configuration
public class RedisConfiguration {

    @Bean
    public RedisTemplate<String, BaseRoom> redisTemplate(RedisConnectionFactory factory) {
        RedisTemplate<String, BaseRoom> template = new RedisTemplate<>();
        template.setConnectionFactory(factory);
        template.setKeySerializer(new StringRedisSerializer());
        template.setValueSerializer(new RedisObjectSerializer());
        return template;
    }
}
